package com.flowid.t3.ui;

import java.util.List;
import java.util.Optional;

public enum Player implements Comparable<Player> {
    O('o'), B('b'), X('x');

    private final char asChar;

    public char asChar() {
        return asChar;
    }

    Player(char c) {
        this.asChar = c;
    }

    public static Player fromChar(char c) {
        for (Player p : Player.values()) {
            if (p.asChar == c) {
                return p;
            }
        }
        return null;
    }

    public static Player next(Player p) {
        return p == X ? O : X;
    }

    public static Optional<Player> min(List<Player> list) {
        return list.stream().min(Player::compareTo);
    }

    public static Optional<Player> max(List<Player> list) {
        return list.stream().max(Player::compareTo);
    }
}
