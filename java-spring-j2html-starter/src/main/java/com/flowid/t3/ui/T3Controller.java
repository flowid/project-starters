package com.flowid.t3.ui;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.flowid.t3.ui.views.T3GridView;
import com.flowid.t3.ui.views.T3PageTemplate;

@Controller
public class T3Controller {
    private static final String TIC_TAC_TOE_TITLE = "Tic Tac Toe";

    private static final Logger logger = LoggerFactory.getLogger(T3Controller.class);

    @Autowired
    T3PageTemplate template;

    @GetMapping("/app/t3")
    public ResponseEntity<String> getTicTacToeForm() {
        Grid g = new Grid();
        return ResponseEntity.ok(template.view(TIC_TAC_TOE_TITLE, T3GridView.view(g)));
    }

    @PostMapping(value = "/app/t3")
    public ResponseEntity<String> nextAction(@RequestParam(value = "grid", required = false) String gs,
        @RequestParam(value = "t3-cell", required = false) String cs) {
        Grid g;

        // first time access oo the page, render an empty grid
        if (StringUtils.isEmpty(cs)) {
            g = new Grid();
            return ResponseEntity.ok(template.view(TIC_TAC_TOE_TITLE, T3GridView.view(g)));
        }

        // Grid already exists, determine the next action
        logger.debug("Next action: {}, {}", gs, cs);
        g = Grid.fromString(gs);
        Player human = g.turn();
        String[] parts = cs.split("\\.");
        int x = Integer.parseInt(parts[0]);
        int y = Integer.parseInt(parts[1]);

        g = g.move(x, y, human);
        if (g.endOfGame()) {
            return ResponseEntity.ok(template.view(TIC_TAC_TOE_TITLE, T3GridView.view(g)));
        }

        Player machine = Player.next(human);
        Grid g2 = Grid.bestMove(g, machine);
        logger.debug("Best move: {}", g2.asString());

        return ResponseEntity.ok(template.view(TIC_TAC_TOE_TITLE, T3GridView.view(g2)));
    }
}
