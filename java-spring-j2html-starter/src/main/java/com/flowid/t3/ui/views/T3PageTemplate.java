package com.flowid.t3.ui.views;

import static j2html.TagCreator.style;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.flowid.ui.views.Col2PageLayout;
import com.flowid.ui.views.CoreUiFragments;
import com.flowid.ui.views.NavGroup;
import com.flowid.ui.views.NavItem;
import com.flowid.ui.views.SidebarView;

import j2html.tags.ContainerTag;
import j2html.tags.specialized.DivTag;

@Component
public class T3PageTemplate {
    private Col2PageLayout CORE_UI_PAGE_VIEW = new Col2PageLayout()
        .withStyles(CoreUiFragments.coreUiFreeStyles())
        .withScripts(CoreUiFragments.coreUiFreeScripts())
        .withStyles(
            style(
                """
                .t3-cell {
                        border: 0
                      }
                """));

    public String view(String title, DivTag cardBody) {
        ContainerTag<?> sidebar = SidebarView.view("Idea...list", "", buildSidebarNavGroups());
        return CORE_UI_PAGE_VIEW.view("Tic Tac Toe", null, sidebar, cardBody, null);
    }

    private static List<NavGroup> buildSidebarNavGroups() {
        NavGroup games = new NavGroup("Games", Arrays.asList(
            new NavItem("tic-tac-toe", "Tic Tac Toe", "/app/t3")));
        return Arrays.asList(games);
    }

}
