package com.flowid.t3.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Grid {
    private static final Logger logger = LoggerFactory.getLogger(Grid.class);
    public static int N = 3;

    private List<List<Player>> ps;

    public Grid() {
        empty();
    }

    public void empty() {
        Supplier<List<Player>> rowGenerator = () -> Stream.generate(() -> Player.B)
            .limit(N)
            .collect(Collectors.toCollection(ArrayList::new));
        ps = Stream.generate(rowGenerator)
            .limit(N)
            .collect(Collectors.toCollection(ArrayList::new));
    }

    // in-place updates
    private Grid setPlayer(int x, int y, Player p) {
        ps.get(x).set(y, p);
        return this;
    }

    public Player getPlayer(int x, int y) {
        return ps.get(x).get(y);
    }

    public String asString() {
        return ps.stream()
            .flatMap(r -> r.stream())
            .map(Player::asChar)
            .map(c -> Character.toString(c))
            .collect(Collectors.joining());
    }

    public static Grid fromString(String s) {
        String snb = s.replace(" ", "");
        Grid g = new Grid();
        IntStream.range(0, snb.length())
            .forEach(i -> {
                g.setPlayer(i / N, i % N, Player.fromChar(snb.charAt(i)));
            });
        return g;
    }

    public Grid transpose() {
        Grid g = new Grid();
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                g.setPlayer(i, j, this.getPlayer(j, i));
            }
        }
        return g;
    }

    @Override
    public Grid clone() {
        return Grid.fromString(this.asString());
    }

    public boolean full() {
        return ps.stream()
            .flatMap(r -> r.stream())
            .allMatch(p -> p != Player.B);
    }

    public List<List<Player>> getRows() {
        return ps;
    }

    public List<List<Player>> getColumns() {
        return transpose().getRows();
    }

    public List<List<Player>> getDiagonals() {
        List<Player> d1 = IntStream.range(0, N)
            .mapToObj(i -> getPlayer(i, i))
            .toList();
        List<Player> d2 = IntStream.range(0, N)
            .mapToObj(i -> getPlayer(i, N - i - 1))
            .toList();
        return Arrays.asList(d1, d2);
    }

    public Player turn() {
        long os = ps.stream()
            .flatMap(r -> r.stream())
            .filter(p -> p == Player.O)
            .count();
        long xs = ps.stream()
            .flatMap(r -> r.stream())
            .filter(p -> p == Player.X)
            .count();
        return os <= xs ? Player.O : Player.X;
    }

    private boolean wins(Player p, List<Player> line) {
        return line.stream()
            .allMatch(lp -> lp == p);
    }

    public boolean wins(Player p) {
        Stream<List<Player>> allLines = Stream.of(getRows(), getColumns(), getDiagonals())
            .flatMap(ll -> ll.stream());
        return allLines.anyMatch(line -> wins(p, line));
    }

    public boolean won() {
        return wins(Player.O) || wins(Player.X);
    }

    public boolean endOfGame() {
        return won() || full();
    }

    public Grid move(int x, int y, Player p) {
        if (getPlayer(x, y) == Player.B) {
            Grid g2 = this.clone();
            g2.setPlayer(x, y, p);
            return g2;
        }

        throw new RuntimeException("Not allowed to move to " + x + "/" + y);
    }

    public static List<Grid> moves(Grid g, Player p) {
        if (g.won() || g.full()) {
            return Collections.emptyList();
        }
        return IntStream.range(0, N * N)
            .filter(i -> g.getPlayer(i / N, i % N) == Player.B)
            .mapToObj(i -> {
                Grid newGrid = g.clone();
                newGrid.setPlayer(i / N, i % N, p);
                return newGrid;
            })
            .toList();
    }

    public static GameTree gameTree(Grid g, Player p) {
        List<GameTree> children = moves(g, p)
            .stream()
            .map(cg -> gameTree(cg, Player.next(p)))
            .toList();
        return new GameTree(new GameNode(g, Player.B), children);
    }

    public static String printGrid(Grid g) {
        return g.getRows()
            .stream()
            .map(row -> row.stream()
                .map(p -> p.toString())
                .collect(Collectors.joining("|")))
            .collect(Collectors.joining("\n"));
    }

    public static Grid bestMove(Grid g, Player p) {
        GameTree tree = Grid.gameTree(g, p).minmax();
        Grid next = tree.getChildren()
            .stream()
            .sorted((c1, c2) -> {
                return c2.getRoot().getPlayer().ordinal() - c1.getRoot().getPlayer().ordinal();
            })
            .findAny()
            .map(GameTree::getRoot)
            .map(GameNode::getGrid)
            .orElse(null);
        return next;
    }
}
