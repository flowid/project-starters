package com.flowid.t3.ui.views;

import static j2html.TagCreator.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.flowid.t3.ui.Grid;
import com.flowid.t3.ui.Player;
import com.flowid.ui.views.CardFragments;
import com.flowid.ui.views.FormFragments;
import com.flowid.ui.views.UserMessagesView;

import j2html.TagCreator;
import j2html.tags.ContainerTag;
import j2html.tags.DomContent;
import j2html.tags.specialized.DivTag;

public class T3GridView {
    private static final DomContent ICON_O = imageO();
    private static final DomContent ICON_X = imageX();
    private static final DomContent ICON_B = imageB();

    private static final String T3_URL = "/app/t3";

    private static final List<Integer> numbers = IntStream.range(0, Grid.N)
        .boxed()
        .collect(Collectors.toList());

    public static DivTag view(Grid grid) {
        boolean endOfGame = grid.endOfGame();
        return CardFragments.card("Play Tic Tac Toe",
            div().with(
                TagCreator.iff(endOfGame, UserMessagesView.success("Game Over! " + endOfGameMessage(grid))),
                FormFragments.form("t3", T3_URL)
                    .with(
                        FormFragments.formGroup()
                            .with(
                                each(numbers, i -> div()
                                    .withClass("row")
                                    .with(
                                        each(numbers, j -> createButton(i, j, grid.getPlayer(i, j)))))),
                        FormFragments.hiddenField("grid", "grid", grid.asString()),
                        FormFragments.formGroup()
                            .with(
                                div()
                                    .withClass("row d-grid gap-2 d-md-block")
                                    .with(
                                        button("Start Again!")
                                            .withClass("btn btn-primary offset-2 col-sm-8")
                                            .withId("button.submit")))

                    )));
    }

    private static String endOfGameMessage(Grid g) {
        if (g.wins(Player.O)) {
            return "Player 0 wins!";
        }
        else if (g.wins(Player.X)) {
            return "Player X wins!";
        }
        else {
            return "It's a draw!";
        }
    }

    private static DomContent createButton(int x, int y, Player p) {
        DomContent svg;
        switch (p) {
        case X -> svg = ICON_X;
        case O -> svg = ICON_O;
        default -> svg = ICON_B;
        }

        return createButton("" + x + "." + y, svg);
    }

    private static DomContent createButton(String id, DomContent svg) {
        return TagCreator.button()
            .withFormaction(T3_URL)
            .withType("submit")
            .withId("t3-cell-" + id)
            .withName("t3-cell")
            .withValue(id)
            .withClass("btn t3-cell col-sm-4")
            // .attr("onClick", "t3CellClicked(this)")
            .with(svg);
    }

    public static ContainerTag imageO() {
        ContainerTag svg = (ContainerTag) (new ContainerTag("svg")
            .attr("height", 100)
            .attr("width", 100));
        svg
            .with(
                TagCreator.join(
                    new ContainerTag("circle")
                        .attr("cx", 50)
                        .attr("cy", 50)
                        .attr("r", 40)
                        .attr("stroke", "black")
                        .attr("stroke-width", 8)
                        .attr("fill", "white"),
                    "Sorry, your browser does not support inline SVG. ")

            //
            );
        return svg;
    }

    public static ContainerTag imageX() {
        ContainerTag svg = (ContainerTag) (new ContainerTag("svg")
            .attr("height", 100)
            .attr("width", 100));
        svg
            .with(
                new ContainerTag("line")
                    .attr("x1", 10)
                    .attr("y1", 10)
                    .attr("x2", 90)
                    .attr("y2", 90)
                    .attr("stroke", "black")
                    .attr("stroke-width", 8),
                new ContainerTag("line")
                    .attr("x1", 90)
                    .attr("y1", 10)
                    .attr("x2", 10)
                    .attr("y2", 90)
                    .attr("stroke", "black")
                    .attr("stroke-width", 8)
            //
            );
        return svg;
    }

    public static ContainerTag imageB() {
        ContainerTag svg = (ContainerTag) (new ContainerTag("svg")
            .attr("height", 100)
            .attr("width", 100));
        svg
            .with(
                new ContainerTag("rect")
                    .attr("x", 5)
                    .attr("y", 5)
                    .attr("height", 90)
                    .attr("width", 90)
                    .attr("fill", "white")
                    .attr("stroke", "black")
                    .attr("stroke-width", 8)
            //
            );
        return svg;
    }

}
