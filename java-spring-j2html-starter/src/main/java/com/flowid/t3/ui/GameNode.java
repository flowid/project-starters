package com.flowid.t3.ui;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GameNode {
    private Grid grid;
    private Player player;
}
