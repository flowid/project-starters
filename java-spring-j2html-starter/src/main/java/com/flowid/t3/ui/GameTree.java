package com.flowid.t3.ui;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GameTree {
    private GameNode root;
    private List<GameTree> children = new ArrayList<>();

    public GameTree minmax() {
        minmax(this);
        return this;
    }

    public static void minmax(GameTree tree) {
        Player winner;
        Grid g = tree.getRoot().getGrid();
        if (tree.getChildren().isEmpty()) { // this is a leaf
            if (g.wins(Player.O)) {
                winner = Player.O;
            }
            else if (g.wins(Player.X)) {
                winner = Player.X;
            }
            else {
                winner = Player.B;
            }
        }
        else { // this is an inner node
            tree.getChildren().stream()
                .forEach(ct -> {
                    minmax(ct);
                });
            List<Player> children = tree.getChildren().stream().map(c -> c.getRoot().getPlayer()).toList();
            if (g.turn() == Player.O) {
                winner = Player.min(children).orElseThrow();
            }
            else {
                winner = Player.max(children).orElseThrow();
            }
        }
        tree.getRoot().setPlayer(winner);
    }
}
