package com.flowid.ui.views;

import static j2html.TagCreator.*;

import java.util.function.Function;

import j2html.TagCreator;
import j2html.tags.specialized.DivTag;
import j2html.tags.specialized.FormTag;
import j2html.tags.specialized.InputTag;

public class FormFragments {

    private static final String LABEL_CLASS = "col-sm-2 col-form-label";

    public static FormTag form(String id, String action) {
        return TagCreator.form()
                .withId(id)
                .withClass("form-horizontal")
                .attr("role", "form")
                .withMethod("POST")
                .withAction(action);
    }

    public static DivTag formGroup() {
        return div().withClass("form-group mb-3 row");
    }

    public static DivTag fileInput(String id, String labelText) {
        return fileInput(id, labelText, Function.identity());
    }

    public static DivTag fileInput(String id, String labelText, Function<InputTag, InputTag> customization) {
        InputTag input = input()
                .withClass("form-control ms-2")
                .withType("file")
                .withId(id)
                .withName(id);
        input = customization.apply(input);
        return formGroup()
                .with(input);
    }

    public static DivTag textInputFormGroup(String id, String labelText, String content) {
        return textInputFormGroup(id, labelText, content, Function.identity());
    }

    public static InputTag hiddenField(String id, String name, String value) {
        return input()
            .isHidden()
            .withId(id)
            .withName(name)
            .withValue(value);
    }

    public static DivTag textInputFormGroup(String id, String labelText, String content,
            Function<InputTag, InputTag> customizations) {
        InputTag input = input()
                .withClass("col-sm-10")
                .withValue(content)
                .withId(id)
                .withName(id);
        input = customizations.apply(input);
        return formGroup()
                .with(
                        label(labelText)
                                .withClass(LABEL_CLASS)
                                .withFor(id),
                        input);
    }

    public static DivTag textPasswordFormGroup(String id, String labelText, String content) {
        return textInputFormGroup(id, labelText, content, input -> input
                .withType("password")
                .isRequired());
    }

    public static DivTag textareaFormGroup(String id, String labelText, int numRows, String content) {
        return formGroup()
                .with(
                        label(labelText)
                                .withClass(LABEL_CLASS)
                                .withFor(id),
                        textarea()
                                .withClass("col-sm-10")
                                .withText(content)
                                .withId(id)
                                .withName(id)
                                .withRows(Integer.toString(numRows)));
    }

    public static DivTag submitFormGroup(String actionName, String cancelHref) {
        return formGroup()
                .with(
                        div()
                                .withClass("d-grid gap-2 d-md-block")
                                .with(
                                        button(actionName)
                                                .withClass("btn btn-primary me-md-2")
                                                .withId("button.submit"),
                                        a("Cancel")
                                                .withClass("btn btn-secondary")
                                                .withHref(cancelHref)
                                                .withId("button.cancel")));
    }

}
