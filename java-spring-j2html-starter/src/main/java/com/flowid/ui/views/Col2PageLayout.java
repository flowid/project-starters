package com.flowid.ui.views;

import static j2html.TagCreator.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import j2html.tags.ContainerTag;
import j2html.tags.DomContent;
import j2html.tags.specialized.DivTag;
import j2html.tags.specialized.HeadTag;
import j2html.tags.specialized.LinkTag;

public class Col2PageLayout {
    private static final LinkTag MANIFEST_LINK = link().withRel("manifest").withHref("/assets/favicon/manifest.json");
    private static final LinkTag ROBOT_CONDENSED_FONT = link().withRel("stylesheet").withHref("https://fonts.googleapis.com/css?family=Roboto Condensed");

    private List<DomContent> coreStyles = new ArrayList<>();
    private List<DomContent> coreScripts = new ArrayList<>();

    public Col2PageLayout withStyles(DomContent... styles) {
        return withStyles(Arrays.asList(styles));
    }

    public Col2PageLayout withStyles(List<DomContent> styles) {
        this.coreStyles.addAll(styles);
        return this;
    }

    public Col2PageLayout withScripts(List<DomContent> scripts) {
        this.coreScripts.addAll(scripts);
        return this;
    }

    public Col2PageLayout withScripts(DomContent... scripts) {
        return withScripts(Arrays.asList(scripts));
    }

    public String view(String title, String userId, ContainerTag sidebar, DivTag cardBody, DomContent script) {
        return document(html(
            getHead(title, coreStyles),
            body(
                sidebar,
                div(
                    attrs(".wrapper .d-flex .flex-column .min-vh-100 .bg-light .bg-opacity-50"),
                    header(
                        attrs(".header .header-light .header-sticky .mb-4"))
                            .with(
                                div()
                                    .withClass("container-fluid")
                                    .with(
                                        renderHamburgerMenu(),
                                        ul()
                                            .withClass("header-nav d-none d-md-flex"),
                                        renderNotifications(),
                                        renderAvatar(userId)
                                    //
                                    )
                            //
                            ),
                    div(
                        attrs(".body .flex-grow .px-3"),
                        div(
                            attrs(".container-lg"),
                            div(
                                attrs(".row"),
                                cardBody)))),
                each(coreScripts, Function.identity()),
                script)));
    }

    public static HeadTag getHead(String title, List<DomContent> styles) {
        return head()
            .with(base().withHref("./"),
                meta().withCharset("utf-8"),
                meta().attr("Http-equiv", "X-UA-Compatible").withContent("IE=edge"),
                meta().withName("viewport").withContent("width=device-width, initial-scale=1.0, shrink-to-fit=no"),
                meta().withName("msapplication-TileColor").withContent("#ffffff"),
                meta().withName("msapplication-TileImage").withContent("/assets/favicon/ms-icon-144x144.png"),
                meta().withName("theme-color").withContent("#ffffff"),
                title(title),
                each(CoreUiFragments.getHeadIconsLinks(), Function.identity()),
                ROBOT_CONDENSED_FONT,
                MANIFEST_LINK,
                each(styles, Function.identity()));
    }

    private static DomContent renderHamburgerMenu() {
        return button()
            .attr("onClick", "coreui.Sidebar.getInstance(document.querySelector('#sidebar')).toggle()")
            .withClass("header-toggler px-md-0 me-md-3 d-md-none")
            .withType("button")
            .with(
                IconFragments.MENU
                    .withClass("icon icon-lg"));
    }

    private static DomContent renderNotifications() {
        return ul()
            .withClass("header-nav ms-auto");
    }

    private static DomContent renderAvatar(String userId) {
        return

        ul()
            .withClass("header-nav me-4")
            .with(
                iff(userId != null, li()
                    .withClass("nav-item dropdown d-flex align-items-center")
                    .with(
                        a()
                            .withClass("nav-link py-0 show")
                            .attr("data-coreui-toggle", "dropdown")
                            .withHref("#")
                            .attr("role", "button")
                            .attr("aria-haspopup", "true")
                            .attr("aria-expanded", "true")
                            .with(
                                div()
                                    .withClass("avatar avatar-md")
                                    .with(span().withText(userId))),
                        div()
                            .withClass("dropdown-menu dropdown-menu-end pt-0")
                            .withStyle(
                                "position: absolute; inset: 0px 0px auto auto; margin: 0px; transform: translate3d(0px, 42px, 0px);")
                            .attr("data-popper-placement", "bottom-end")
                            .with(
                                a()
                                    .withClass("dropdown-item")
                                    .withHref("/app/logout")
                                    .with(join(
                                        IconFragments.ACCOUNT_LOGOUT_ICON,
                                        "Logout"))))));
    }

}
