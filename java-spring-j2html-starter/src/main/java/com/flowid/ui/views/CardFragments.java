package com.flowid.ui.views;

import static j2html.TagCreator.*;

import java.util.Collections;
import java.util.List;

import j2html.tags.DomContent;
import j2html.tags.Tag;
import j2html.tags.specialized.DivTag;
import lombok.AllArgsConstructor;
import lombok.Data;

public class CardFragments {
    @Data
    @AllArgsConstructor
    public static class LabeledLink {
        String id;
        String label;
        String link;
        String level; // primary, secondary
    }

    static Tag<?> viewMenuItem(LabeledLink item) {
        return a()
            .withId(item.getId())
            .withType("button")
            .withHref(item.getLink())
            .withText(item.getLabel())
            .withClass("btn btn-sm btn-" + item.getLevel());

    }

    static public DivTag card(String cardTitle, DomContent cardBodyView) {
        return card(cardTitle, Collections.emptyList(), cardBodyView);
    }

    static public DivTag card(String cardTitle, List<LabeledLink> menu, DomContent cardBodyView) {
        return div()
            .withClass("card mb-r")
            .with(
                div(
                    attrs(".card-header"),
                    join(h3(cardTitle).withClass("col-sm-6"),
                        iff(menu.size() > 0,
                            div()
                                .withClass("d-grid gap-2 d-md-flex justify-content-md-end")
                                .with(
                                    each(menu, item -> viewMenuItem(item))
                                //
                                )
                        //
                        ))),
                div(
                    attrs(".card-body"), cardBodyView));
    }
}
