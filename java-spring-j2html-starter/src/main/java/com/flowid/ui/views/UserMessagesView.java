package com.flowid.ui.views;

import static j2html.TagCreator.*;

import j2html.tags.specialized.DivTag;

public class UserMessagesView {

    public static DivTag error(String cardTitle, String message) {
        return CardFragments.card(cardTitle, div()
            .withClass("alert alert-danger alert-dismissible fade show")
            .attr("role", "alert")
            .with(
                p(message)));
    };

    public static DivTag success(String cardTitle, String message) {
        return CardFragments.card(cardTitle, success(message));
    }

    public static DivTag success(String message) {
        return div()
            .withClass("alert alert-success alert-dismissible fade show")
            .attr("role", "alert")
            .with(
                p(message));
    }

    public static DivTag createYesNoModal(String id, String message, String actionName, String actionUrl, Boolean hidden) {
        return div()
                .withClass("modal fade")
                .withId(id)
                .withTabindex(-1)
                .attr("aria-labelledby", "delete-entity-modal")
            .attr("aria-hidden", hidden)
                .with(div()
                        .withClass("modal-dialog")
                        .with(div()
                                .withClass("modal-content")
                                .with(div()
                                        .withClass("modal-header")
                                        .with(h5()
                                                .withClass("modal-title")
                                                .withText(message),
                                                button()
                                                        .withType("button")
                                                        .withClass("btn-close")
                                                        .attr("data-coreui-dismiss", "modal")
                                                        .attr("aria-label", "Close")),
                                        div()
                                                .withClass("modal-footer")
                                                .with(
                                                        button()
                                                                .withType("button")
                                                                .withClass("btn btn-secondary")
                                                                .attr("data-coreui-dismiss", "modal")
                                                                .withText("No"),
                                                        a()
                                                                .withType("button")
                                                                .withClass("btn btn-primary")
                                                                .withHref(actionUrl)
                                                                .withText("Yes")))))

        ;

    };

}
