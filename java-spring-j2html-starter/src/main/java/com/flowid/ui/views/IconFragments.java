package com.flowid.ui.views;

import j2html.tags.ContainerTag;
import j2html.tags.Tag;

public class IconFragments {
    public static ContainerTag icon(String name) {
        ContainerTag svg = (ContainerTag) (new ContainerTag("svg")
                .withClasses("icon"));

        svg.with(
                new ContainerTag("use")
                        .attr("href", "/vendors/@coreui/icons/svg/free.svg#" + name)
        //
        );
        return svg;
    }

    public static Tag TRASH_ICON = icon("cil-trash");
    public static Tag ACCOUNT_LOGOUT_ICON = icon("cil-account-logout");
    public static Tag MENU = icon("cil-menu");
}
