package com.flowid.ui.views;

import j2html.TagCreator;
import j2html.tags.specialized.DivTag;

public class PreformattedCardView {
    public static DivTag view(String cardTitle, String preFormattedContent) {
        return CardFragments.card(cardTitle, TagCreator.pre(preFormattedContent));
    };
}
