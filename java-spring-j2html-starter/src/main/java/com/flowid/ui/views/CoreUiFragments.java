package com.flowid.ui.views;

import static j2html.TagCreator.*;

import java.util.Arrays;
import java.util.List;

import j2html.tags.DomContent;
import j2html.tags.specialized.LinkTag;

public class CoreUiFragments {

    public static List<DomContent> coreUiFreeStyles() {
        return Arrays.asList(
            link()
                .withRel("stylesheet")
                .withHref("/css/vendors/simplebar.css"),
            link()
                .withHref("/css/style.css")
                .withRel("stylesheet"));
    }

    public static List<DomContent> coreUiFreeScripts() {
        return Arrays.asList(
            script().withSrc("https://cdn.jsdelivr.net/npm/@coreui/coreui@4.3.0/dist/js/coreui.bundle.min.js"));
    }

    public static List<DomContent> coreUiProStyles() {
        return Arrays.asList(
            link()
                .withRel("stylesheet")
                .withHref("/css/vendors/simplebar.css"),
            link()
                .withHref("/css/style.css")
                .withRel("stylesheet"),
            link()
                .withHref("/vendors/datatables.net-bs4/css/dataTables.bootstrap4.css")
                .withRel("stylesheet"));
    }

    public static List<DomContent> coreUiProScripts() {
        return Arrays.asList(
            script().withSrc("/vendors/@coreui/coreui-pro/js/coreui.bundle.min.js"),
            script().withSrc("/vendors/simplebar/js/simplebar.min.js"));
    }

    public static List<LinkTag> getHeadIconsLinks() {
        return Arrays.asList(
            link()
                .withRel("apple-touch-icon")
                .withSizes("57x57")
                .withHref("/assets/favicon/apple-icon-57x57.png"),
            link()
                .withRel("apple-touch-icon")
                .withSizes("60x60")
                .withHref("/assets/favicon/apple-icon-60x60.png"),
            link()
                .withRel("apple-touch-icon")
                .withSizes("72x72")
                .withHref("/assets/favicon/apple-icon-72x72.png"),
            link()
                .withRel("apple-touch-icon")
                .withSizes("76x76")
                .withHref("/assets/favicon/apple-icon-76x76.png"),
            link()
                .withRel("apple-touch-icon")
                .withSizes("114x114")
                .withHref("/assets/favicon/apple-icon-114x114.png"),
            link()
                .withRel("apple-touch-icon")
                .withSizes("120x120")
                .withHref("/assets/favicon/apple-icon-120x120.png"),
            link()
                .withRel("apple-touch-icon")
                .withSizes("144x144")
                .withHref("/assets/favicon/apple-icon-144x144.png"),
            link()
                .withRel("apple-touch-icon")
                .withSizes("152x152")
                .withHref("/assets/favicon/apple-icon-152x152.png"),
            link()
                .withRel("apple-touch-icon")
                .withSizes("180x180")
                .withHref("/assets/favicon/apple-icon-180x180.png"),
            link()
                .withRel("icon")
                .withType("image/png")
                .withSizes("192x192")
                .withHref("/assets/favicon/android-icon-192x192.png"),
            link()
                .withRel("icon")
                .withType("image/png")
                .withSizes("32x32")
                .withHref("/assets/favicon/favicon-32x32.png"),
            link()
                .withRel("icon")
                .withType("image/png")
                .withSizes("96x96")
                .withHref("/assets/favicon/favicon-96x96.png"),
            link()
                .withRel("icon")
                .withType("image/png")
                .withSizes("16x16")
                .withHref("/assets/favicon/favicon-16x16.png"));
    }

}
