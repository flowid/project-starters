package com.flowid.ui.views;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NavItem {
    private String id;
    private String label;
    private String href;
}