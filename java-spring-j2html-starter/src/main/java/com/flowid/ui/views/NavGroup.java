package com.flowid.ui.views;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class NavGroup {
    private String label;
    private List<NavItem> items;
    private List<String> roles = new ArrayList<>();

    public NavGroup(String label, List<NavItem> items) {
        this.label = label;
        this.items = items;
    }

    public NavGroup withRole(String role) {
        roles.add(role);
        return this;
    }
}