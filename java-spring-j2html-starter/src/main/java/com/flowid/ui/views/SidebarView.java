package com.flowid.ui.views;

import static j2html.TagCreator.*;

import java.util.List;
import java.util.stream.Stream;

import j2html.tags.ContainerTag;
import j2html.tags.DomContent;

public class SidebarView {

    public static ContainerTag viewBrand(String title, String env) {
        return div(
            attrs(".sidebar-brand .d-none .d-md-flex"),
            div(h4(title)));

    }

    public static ContainerTag viewGroups(List<NavGroup> groups) {
        return ul()
            .withClass("simple-sidebar")
            .withStyle("padding: 0px;")
            .with(groups.stream()
                .flatMap(SidebarView::viewGroup));
    }

    public static Stream<DomContent> viewGroup(NavGroup group) {
        return Stream.concat(

            Stream.of(li(group.getLabel()).withClass("nav-title")),

            Stream.of(
                each(group.getItems(), item -> li()
                    .withClass(".nav-item")
                    .withId("sidebar-" + item.getId())
                    .with(
                        a()
                            .withClass("nav-link")
                            .withHref(item.getHref())
                            .with(join(
                                span().withClass("nav-icon"),
                                item.getLabel()))))))

        ;
    }

    public static ContainerTag viewSideBarNav(List<NavGroup> groups) {
        return div()
            .withClass("sidebar-nav")
            .attr("data-coreui", "")
            .attr("data-sidebar", "init")
            .with(
                div()
                    .withClass("simplebar-wrapper")
                    .withStyle("margin: 0px;")
                    .with(
                        div()
                            .withClass("simplebar-height-auto-observer-wrapper")
                            .with(
                                div()
                                    .withClass("simplebar-height-auto-observer")),
                        div(
                            attrs(".simplebar-mask"),
                            div()
                                .withClass("simplebar-ofset")
                                .withStyle("right: 0px; bottom: 0px;")
                                .with(
                                    div()
                                        .withClass("simple-bar-content-wrapper")
                                        .withStyle("heigth: 100%; overflow: hidden")
                                        .with(
                                            viewGroups(groups)))),
                        div()
                            .withClass("simplebar-track simplebar-horizontal")
                            .withStyle("visibility: hidden;")
                            .with(
                                div()
                                    .withClass("simplebar-scrollbar")
                                    .withStyle("width: 0px; display: none;")),
                        div()
                            .withClass("simplebar-track simplebar-vertical")
                            .withStyle("visibility: hidden;")
                            .with(
                                div()
                                    .withClass("simplebar-scrollbar")
                                    .withStyle(
                                        "height: 0px; transform: translate3d(0px, 0px, 0px); display: none;"))));
    }

    public static ContainerTag view(String title, String env, List<NavGroup> navGroups) {
        return div()
            .withId("sidebar")
            .withClass("sidebar sidebar-light sidebar-fixed")
            .with(
                viewBrand(title, env),
                viewSideBarNav(navGroups));
    }
}
