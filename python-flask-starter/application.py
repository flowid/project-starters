from flask import Flask, request, render_template, redirect, url_for
import os

application = Flask(__name__)

@application.route('/', methods=['GET'])
def index_get():
    return render_template("index.html")

@application.route('/', methods=['POST'])
def index_post():
    return redirect("/calculate")


@application.route("/calculate", methods=['GET'])
def calculate_get():
    return render_template('calculate-form-page.html')

@application.route("/calculate", methods=['POST'])
def calculate_post():

    return render_template('calculate-results-page.html', a = 1, b = 2, result = 3)

if __name__ == '__main__':
    application.run(debug=True, host='localhost',port=5001)
