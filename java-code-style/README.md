The coding style follows the same conventions as described in: 
    https://google.github.io/styleguide/javaguide.html

To install the code styles:

Preferences > Java > Code Style > Formatter
Import > java-code-formatter.xml

To install the code templates:

Preferences > Java > Code Style > Code Templates
Import > java-code-templates.xml

To install editor templates:

Preferences > Java > Editor > Templates
Import > java-editor-templates.xml

Preferences > Java > Editor > Save Actions:
  * Perform the selection actions on save - Checked
    * Format source code - checked
         * Format edited lines
  * Organize imports - checked
  * Additional actions checked:  
    - Add missing '@Override' annotations
    - Add missing '@Override' annotations to implementations of interface methods
    - Remove unnecessary casts
    - Remove redundant type arguments

Preferences > General > Keys
  Search for junit
  Bind the following:
    Run JUnit Test / In Windows / Run/Debug -> Ctrl-1
    Rerun JUnit Test / In Windows / Run/Debug -> Ctrl-2

Refactor - Rename
    Preferences > General > Keys
       Rename - Refactoring - In Windows, Edit Java Source -> F2



