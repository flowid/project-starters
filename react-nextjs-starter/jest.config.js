module.exports = {
  testPathIgnorePatterns: ['/.next/', '/node_modules/'],
  testEnvironment: 'jest-environment-jsdom',
  moduleNameMapper: {
    '\\.module\\.css$': 'identity-obj-proxy',
    '\\.css$': require.resolve('./src/mocks/style-mock.js'),
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/src/mocks/file-mock.js',
  },
  snapshotSerializers: [],

  globalSetup: './global-setup.js',
};
