module.exports = {
  extends: ["./node_modules/poetic/config/eslint/eslint-config.js"],
  rules: {
    "no-use-before-define": "off",
    "@typescript-eslint/no-use-before-define": ["error"],

    'no-underscore-dangle': ["off"],
    '@typescript-eslint/no-unused-vars': ["warn", { "argsIgnorePattern": "^_", "varsIgnorePattern": "^_" }],
    'react/jsx-filename-extension': ['error', { extensions: ['.js', '.jsx', '.ts', '.tsx'] }],
    'react/jsx-fragments': 'off',
    'jsx-a11y/anchor-is-valid': [
      'error',
      {
        components: ['Link'],
        specialLink: ['hrefLeft', 'hrefRight'],
        aspects: ['invalidHref', 'preferButton'],
      },
    ],
    'import/no-extraneous-dependencies': [
      'error',
      { devDependencies: ['**/*.test.tsx', '**/*.spec.js'] },
    ],
  }
};
