import Head from 'next/head';
import React from 'react'
import { Col, Container, Row } from 'react-bootstrap';
import Header from './Header';
import Sidebar from './Sidebar';

interface Props {
    // eslint-disable-next-line react/require-default-props
    children?: React.ReactNode,
    // eslint-disable-next-line react/require-default-props
    title?: string
}

const SiteLayout: React.FC = ({ children = {}, title = '' }: Props) => {
    return (
        <>
            <Head>
                <title>{title}</title>
                <meta charSet="utf-8" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>

            <Header />
            <main>
                <Container fluid>
                    <Row className="flex-xl-nowrap">
                        <Col className="md-3 xl-2 bd-sidebar">
                            <Sidebar />
                        </Col>
                        <main className="col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content">
                            {children}
                        </main>
                    </Row>
                </Container>
            </main>
        </>
    )
};

export default SiteLayout;

