import * as React from 'react';
import { render, cleanup } from '@testing-library/react';

import Status from './Status';

afterEach(cleanup);

describe('<Status/>', () => {
    test('renders', () => {
        const { container } = render(
            <Status />
        );
        expect(container).toMatchSnapshot();
    });
});