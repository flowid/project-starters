import Link from 'next/link';
import React from 'react';
import { Nav, Row } from 'react-bootstrap';

const Sidebar: React.FC = (): React.ReactElement => {
    return (
        <Row className="d-md-block collapse">
            <div className="bd-links">
                <div className="bd-toc-item active">
                    <Link href="/not-implemented?t=Getting_Started" passHref>
                        <Nav.Link className="bd-toc-link">Getting started</Nav.Link>
                    </Link>

                    <ul className="nav bd-sidenav">
                        <li>
                            <Link href="/not-implemented" passHref>
                                <Nav.Link>Introduction</Nav.Link>
                            </Link>
                        </li>
                        <li>
                            <Link href="/not-implemented" passHref>
                                <Nav.Link>Download</Nav.Link>
                            </Link>
                        </li>
                    </ul>
                </div>

                <Link href="/not-implemented?t=Layout" passHref>
                    <Nav.Link className="bd-toc-link">Layout</Nav.Link>
                </Link>
                <Link href="/not-implemented?t=Content" passHref>
                    <Nav.Link className="bd-toc-link">Content</Nav.Link>
                </Link>
            </div>
        </Row >
    );
}

export default Sidebar;
