import React from 'react';
import Head from 'next/head';
import { Col, Row } from 'react-bootstrap';

// Can't destructure or use variables because process.env... is substituted at build time
interface ConfigEntry {
    key: string;
    value: string | undefined;
}

const configEntries: ConfigEntry[] = [
    { key: 'NODE_ENV', value: process.env.NODE_ENV },
    { key: 'ENV', value: process.env.REACT_APP_ENV },
    { key: 'BUILD_TIMESTAMP', value: process.env.REACT_APP_BUILD_TIMESTAMP },
    { key: 'LOG_LEVEL', value: process.env.REACT_APP_LOG_LEVEL },
];

const Status: React.FC = () => {
    return (
        <Row>
            <Head>
                <title>Status</title>
                <meta name="description" content="Status" />
            </Head>
            <Col>
                <table className="table table-bordered">
                    <thead>
                        <tr className="thead-light">
                            <th scope="col">Name</th>
                            <th scope="col">Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        {configEntries.map((configEntry) => {
                            return (
                                <tr key={configEntry.key}>
                                    <td>{configEntry.key}</td>
                                    <td>
                                        <code>{configEntry.value || 'undefined'}</code>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </Col>
        </Row>
    );
};

export default Status;
