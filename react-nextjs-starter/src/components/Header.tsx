import React from 'react';
import Link from 'next/link';
import { Nav, Navbar } from 'react-bootstrap';
import '../../lib/bd-docs.css'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const logo = <FontAwesomeIcon icon={faCoffee} />

const Header: React.FC = (): React.ReactElement => {
    return (
        <header className="navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar">
            <Navbar className="bd-navbar-nav flex-row" expand="lg">
                <Navbar.Brand href="/">{logo}</Navbar.Brand>
                <Nav className="mr-auto">
                    <Link href="/status" passHref>
                        <Nav.Link href="#home">Status</Nav.Link>
                    </Link>
                    <Link href="/status?p=2" passHref>
                        <Nav.Link href="#home">Status2</Nav.Link>
                    </Link>
                </Nav>
            </Navbar>
            <Navbar className="bd-navbar-nav ml-auto" expand="lg">
                <Nav className="mr-auto">
                    <Link href="/status" passHref>
                        <Nav.Link href="#home">Status</Nav.Link>
                    </Link>
                    <Link href="/status?p=2" passHref>
                        <Nav.Link href="#home">Status2</Nav.Link>
                    </Link>
                </Nav>
            </Navbar>

        </header>
    );
};

export default Header;