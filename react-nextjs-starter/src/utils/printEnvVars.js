/*
  This is a very simple script to print the process env vars.
  It is used in package.json to test the environment configuration.
  See: package.json: test-env
 */
// eslint-disable-next-line no-console
console.log(JSON.stringify(process.env, null, 2));
