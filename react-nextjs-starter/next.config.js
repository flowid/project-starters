/* eslint-disable @typescript-eslint/no-var-requires */
// next.config.js
const nextEnv = require('next-env');
const dotenvLoad = require('dotenv-load');
const withSass = require('@zeit/next-sass');
const withImages = require('next-images');
const withLess = require('@zeit/next-less')
const withCSS = require('@zeit/next-css')

dotenvLoad();

const withNextEnv = nextEnv({
  // Your Next.js config.
  staticPrefix: 'REACT_APP_',
});

const nextPlugins = [
  withNextEnv,
  withSass,
  withImages,
  withLess,
  withCSS
];

const withPlugins = require('next-compose-plugins');

module.exports = withPlugins(nextPlugins);
