/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import React from 'react'
import Head from 'next/head'
import { Col, Row } from 'react-bootstrap'
import SiteLayout from '../src/components/SiteLayout'

const Home = (): React.ReactElement => {
  return (
    <SiteLayout>
      <Head>
        <title>Home</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Row>
        <Col>
          <p>Main Page</p>
        </Col>
      </Row>
    </SiteLayout >
  )
}

export default Home;