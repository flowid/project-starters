/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React from 'react';
import { AppProps } from 'next/app';

import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'

// eslint-disable-next-line react/prop-types
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const App = ({ Component, pageProps }: AppProps) => {
  // eslint-disable-next-line react/jsx-props-no-spreading
  return <Component {...pageProps} />
}

export default App;
