import React from 'react';
import { NextPage } from 'next';
import Status from '../src/components/Status';
import SiteLayout from '../src/components/SiteLayout';

const Page: NextPage<void> = () => {
    return (
        <SiteLayout>
            <Status />
        </SiteLayout>
    );
}

export default Page;
