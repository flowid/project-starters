package com.example.ui;

import java.io.IOException;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import htmlflow.HtmlFlow;
import htmlflow.HtmlView;

@Controller
public class MainUiController {
    @RequestMapping("/")
    public ResponseEntity<?> index() throws IOException {
        return ResponseEntity.ok(welcomePageView().render());

    }

    public static HtmlView welcomePageView() {
        return HtmlFlow.view(page -> page
        // @formatter:off
            .html()
                .head()
                .meta().addAttr("http-equiv","Content-Type").attrContent("text/html; charset=UTF-8")
                .__() //meta
                .meta().attrCharset("utf-8")
                .__() //meta
                .meta().addAttr("http-equiv","X-UA-Compatible").attrContent("IE=edge")
                .__() //meta
                .meta().attrName("viewport").attrContent("width=device-width, initial-scale=1")
                .__() //meta
                .link().addAttr("rel","shortcut icon").addAttr("type","image/x-icon").attrHref("/resources/images/favicon.png")
                .__() //link
                .title()
                    .text("Demo: java-spring-htmlflow-starter")
                .__() //title
              .__() //head
            .body()
                .div().attrClass("container")
                  .h1().text("My first page with HtmlFlow").__()
                  .img().attrSrc("http://bit.ly/2MoHwrU").__()
                  .p().text("Typesafe is awesome! :-)").__()
                .__() // div
            .__() //body
         .__() //html
         // @formatter:on
        );
    }

}
