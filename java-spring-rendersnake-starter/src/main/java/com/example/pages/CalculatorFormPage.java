package com.example.pages;

import static org.rendersnake.HtmlAttributesFactory.name;
import static org.rendersnake.HtmlAttributesFactory.type;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

public class CalculatorFormPage implements Renderable {

    @Override
    public void renderOn(HtmlCanvas html) throws IOException {
        html.h1()
                .write("Calculator")
                ._h1();

        html.form(name("donate-form").method("POST").action("calculate"));
        html.fieldset();

        RenderUtils.renderInputField(html, "A", "");
        RenderUtils.renderInputField(html, "B", "");

        html.button(type("submit").class_("btn btn-primary")).content("Submit");

        html._fieldset();
        html._form();
    }
}
