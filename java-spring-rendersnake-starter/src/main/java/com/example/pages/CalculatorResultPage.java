package com.example.pages;

import static org.rendersnake.HtmlAttributesFactory.class_;

import java.io.IOException;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;

public class CalculatorResultPage implements Renderable {

    String userId;
    long a;
    long b;
    long result;

    public CalculatorResultPage(String userId, long a, long b, long result) {
        this.userId = userId;
        this.a = a;
        this.b = b;
        this.result = result;
    }

    @Override
    public void renderOn(HtmlCanvas html) throws IOException {
        html.h1()
                .write("Calculation Result")
                ._h1();

        String msg = String.format("Thank you %s.\nThe sum of %d and %d is %s.",
                userId, a, b, result);
        
        
        html.div(class_("row")).p(class_("large-text")).content(msg)._div();
        
        html.div(class_("row"));
        html.a(class_("btn btn-primary").href("/calculate")).content("New Calculation");
        html.a(class_("btn btn-secondary").href("/")).content("Logout");
        html._div();
    }
}
