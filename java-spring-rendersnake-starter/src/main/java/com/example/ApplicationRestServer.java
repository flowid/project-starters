package com.example;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.rendersnake.HtmlCanvas;
import org.rendersnake.Renderable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.pages.CalculatorFormPage;
import com.example.pages.CalculatorResultPage;
import com.example.pages.LoginPage;
import com.example.pages.PageTemplate;

@Controller
public class ApplicationRestServer {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationRestServer.class);

    @RequestMapping("/")
    public ResponseEntity<?> index() throws IOException {

        HtmlCanvas html = new HtmlCanvas();

        Renderable loginPage = new PageTemplate(new LoginPage());
        html.render(loginPage);

        return ResponseEntity.ok(html.toHtml());
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String loginProcess(@RequestBody MultiValueMap<String, String> fields, HttpSession session) {
        session.setAttribute("user", fields.getFirst("user"));
        logger.info("Sesion initialized");

        return "redirect:/calculate";
    }

    @RequestMapping(value = "/calculate", method = RequestMethod.GET)
    public ResponseEntity<?> calculateGet(Map<String, String> fields) throws IOException {

        HtmlCanvas html = new HtmlCanvas();

        Renderable donateForm = new PageTemplate(new CalculatorFormPage());
        html.render(donateForm);

        return ResponseEntity.ok(html.toHtml());
    }

    @RequestMapping(value = "/calculate", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            method = RequestMethod.POST)
    public ResponseEntity<?> calculatePost(@RequestBody MultiValueMap<String, String> fields,
            HttpSession session) throws IOException {
        logger.info("Received {} fields", fields.size());
        for (String key : fields.keySet()) {
            logger.info("{} -> {}", key, fields.get(key));
        }

        String userId = (String) session.getAttribute("user");
        long a = Long.parseLong(fields.getFirst("a"));
        long b = Long.parseLong(fields.getFirst("b"));

        HtmlCanvas html = new HtmlCanvas();

        Renderable results = new PageTemplate(new CalculatorResultPage(userId, a, b, a + b));
        html.render(results);

        return ResponseEntity.ok(html.toHtml());
    }
}
